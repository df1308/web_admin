// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'

import ElementUI from 'element-ui'
import App from './App'
import router from './router'
import axios from 'axios'
import { post, get, patch, put, del } from './public/http'
//import echarts from './public/echarts'

import 'element-ui/lib/theme-chalk/index.css'
import echarts from 'echarts'
Vue.use(ElementUI)
Vue.use(echarts)
Vue.prototype.$echarts = echarts
Vue.config.productionTip = false
Vue.prototype.$post = post
Vue.prototype.$get = get
Vue.prototype.$patch = patch
Vue.prototype.$put = put
Vue.prototype.$del = del
Vue.prototype.url = 'http://clinit.aikejishu.com'
Vue.prototype.apiUrl = 'http://clinit.aikejishu.com/api/'
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
