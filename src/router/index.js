import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/index'
import Login from '@/pages/login/index'
import User from '@/pages/user/index'
import Menu from '@/pages/menu/index'
import add from '@/pages/menu/add'
import Goods from '@/pages/goods/index'
import Cost from '@/pages/cost/index'
import Supply from '@/pages/supply/index'
import BuyAdd from '@/pages/supply/buy'
import Buy from '@/pages/buy/index'
import UserSale from '@/pages/user/sale'
import Sale from '@/pages/sale/index'
import Form from '@/pages/form/index'
import FormSale from '@/pages/form/sale'
import FormBuy from '@/pages/form/buy'
import FormCost from '@/pages/form/cost'
import FormCheck from '@/pages/form/check'
import FormResult from '@/pages/form/result'
import FormYear from '@/pages/form/year'
import Log from '@/pages/log/index'
import StoreInfo from '@/pages/store/info'
import StorePass from '@/pages/store/pass'
import Staff from '@/pages/staff/index'
Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      name: '首页',
      component: Index
    },
    {
      path: '/login',
      name: '登陆',
      component: Login
    },
    {
      path: '/staff',
      name: '员工管理',
      component: Staff
    },
    {
      path: '/menu',
      name: '菜单管理',
      component: Menu
    },
    {
      path: '/add',
      name: '添加表单',
      component: add
    },
    {
      path: '/user',
      name: '客户管理',
      component: User
    },
    {
      path: '/goods',
      name: '产品管理',
      component: Goods
    },
    {
      path: '/cost',
      name: '支出费用',
      component: Cost
    },
    {
      path: '/supply',
      name: '供应商',
      component: Supply
    },
    {
      path: '/supply/buy',
      name: '新建采购单',
      component: BuyAdd
    },
    {
      path: '/buy',
      name: '采购单',
      component: Buy
    },
    {
      path: '/user/sale',
      name: '新建销售单',
      component: UserSale
    },
    {
      path: '/sale',
      name: '销售单',
      component: Sale
    },
    {
      path: '/form',
      name: '报表',
      component: Form
    },
    {
      path: '/form/sale',
      name: '销售报表',
      component: FormSale
    },
    {
      path: '/form/buy',
      name: '采购报表',
      component: FormBuy
    },
    {
      path: '/form/cost',
      name: '支出报表',
      component: FormCost
    },
    {
      path: '/form/check',
      name: '产品盘点表',
      component: FormCheck
    },
    {
      path: '/form/result',
      name: '员工业绩统计表',
      component: FormResult
    },
    {
      path: '/form/year',
      name: '公司年报',
      component: FormYear
    },
    {
      path: '/log',
      name: '业务日志',
      component: Log
    },
    {
      path: '/store/info',
      name: '商户信息',
      component: StoreInfo
    },
    {
      path: '/store/pass',
      name: '密码重置',
      component: StorePass
    }
  ]
})
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
